// ******** include - Variable ********
//
// author:		Felix Schmidt
//
//
// LOG:
// modified		version		description
// 2017-06-02	1.00		created
// 2017-06-29	1.10		add: Variables with Parameters
// 2017-09-21	1.11		add: file, ini gui
// 2017-11-20	1.12		mod: ELT config
// 2018-02-16	1.20		modified
// 2018-04-06	1.30		mod: INI iProcurement
// 2018-04-07	1.31		add: Log
// 2018-04-10	1.32		mod: Log
// 2018-04-22	1.33		mod Variable: vStdTaskOut
// 2018-04-22	1.34		add Variables: vStdTaskQvdFilesMap, vStdTaskQvdFilesTemplate
// 2018-06-24	1.35		mod: ETL data
//
// ******** include - Variable ********



// **************** **************** **************** ****************;
// *** Date, Time, Format;
// **************** **************** **************** ****************;

LET vStdTimestamp				= Timestamp(Now());
LET vStdTimestampNum			= Timestamp(vStdTimestamp,'YYYYMMDDhhmmss');

SET vStdFormatNumberInt			= "'#$(ThousandSep)##0'";
SET vStdFormatNumberDec			= "'#$(ThousandSep)##0$(DecimalSep)00'";
SET vStdFormatMoneyInt			= "'#$(ThousandSep)##0 �;-#$(ThousandSep)##0 �'";
SET vStdFormatMoneyDec			= "'#$(ThousandSep)##0$(DecimalSep)00 �;-#$(ThousandSep)##0$(DecimalSep)00 �'";
SET vStdFormatTimeInterval		= "'hh:mm:ss'";
SET vStdFormatDateGui			="'DD.MM.YYYYY'";



// **************** **************** **************** ****************;
// *** ETL;
// **************** **************** **************** ****************;


// **************** ****************
// *** name
// **************** ****************

LET vStdPath				= Left(DocumentPath(),Index(DocumentPath(),'\',-4)-1);
LET vStdPathInclude			= vStdPath &'\9_SYSTEM\99_INCLUDE';
LET vStdTaskLevel			= SubField(DocumentPath(),'\',-4);
LET vStdTaskLevelNo			= Left(vStdTaskLevel,1);
LET vStdTaskLevelName		= Pick(vStdTaskLevelNo,'Files','Extraction','Transaction','Datamodel','Application','Report');
LET vStdTaskScope			= Replace(SubField(DocumentPath(),'\',-2),'#','');
LET vStdTaskScopePrefix		= If(Left(SubField(DocumentPath(),'\',-2),1)='#','#','');
LET vStdTaskFile			= Left(DocumentName(),Len(DocumentName())-4);
LET vStdTaskName			= SubField(vStdTaskFile,'-',2);
LET vStdTaskMode			= If(Lower(SubField(vStdTaskName,'.',2))='config','config','default');
LET vStdTaskPrefix			= If(Index(vStdTaskName,'.'),SubField(vStdTaskName,'.',1),vStdTaskScope);
LET vStdTaskSuffix			= If(Len(SubField(vStdTaskFile,'-',3))>0,If(Lower(SubField(vStdTaskFile,'-',3))='connect' and vStdTaskLevelNo=2,'connect','undefined'),'none');


// **************** ****************
// *** qvs
// **************** ****************

LET vStdTaskQvs					= Left(DocumentPath(),Index(DocumentPath(),'\',-3)) & vStdTaskLevelNo & '2_QVS';
LET vStdTaskQvsStandardPrefix	= vStdTaskQvs & '\#standard\' & vStdTaskLevelNo & '_standard';
LET vStdTaskQvsScopePrefix		= vStdTaskQvs & '\' & vStdTaskScopePrefix & vStdTaskScope & '\' & vStdTaskLevelNo & '_' & vStdTaskScope;


// **************** ****************
// *** data
// **************** ****************

LET vStdTaskDiv					= If(Match(vStdTaskLevelNo,1,2,6),Left(DocumentPath(),Index(DocumentPath(),'\',-3)) & vStdTaskLevelNo &'8_DIV\'& vStdTaskScopePrefix & vStdTaskScope,Null());
LET vStdTaskQvd					= If(Match(vStdTaskLevelNo,1,2,3,6),Left(DocumentPath(),Index(DocumentPath(),'\',-3)) & vStdTaskLevelNo &'9_QVD\'& vStdTaskScopePrefix & vStdTaskScope,Null());
LET vStdTaskOut					= If(Match(vStdTaskLevelNo,1,2,3,6),Left(DocumentPath(),Index(DocumentPath(),'\',-3)) & vStdTaskLevelNo &'7_OUT\'& vStdTaskScopePrefix & vStdTaskScope,Null());
LET vStdTaskQvdFiles			= If(Match(vStdTaskLevelNo,1,3,4,5,6),vStdPath &'\1_FILES\19_QVD\'& vStdTaskScopePrefix & vStdTaskScope,Null());
LET vStdTaskQvdExtract			= If(Match(vStdTaskLevelNo,2,3,4,5,6),vStdPath &'\2_EXTRACT\29_QVD\'& vStdTaskScopePrefix & vStdTaskScope,Null());
LET vStdTaskQvdTransact			= If(Match(vStdTaskLevelNo,3,4,5,6),vStdPath &'\3_TRANSACT\39_QVD\'& vStdTaskScopePrefix & vStdTaskScope,Null());
LET vStdTaskOutTransact			= If(Match(vStdTaskLevelNo,3,4,5),vStdPath &'\3_TRANSACT\37_OUT\'& vStdTaskScopePrefix & vStdTaskScope,Null());
LET vStdTaskOutReport			= If(vStdTaskLevelNo=6,vStdPath &'\6_REPORT\67_OUT\'& vStdTaskScopePrefix & vStdTaskScope,Null());

LET vStdTaskQvdFilesData		= vStdPath &'\1_FILES\19_QVD\#data';
LET vStdTaskQvdFilesMap			= vStdPath &'\1_FILES\19_QVD\#map';
LET vStdTaskQvdFilesTemplate	= vStdPath &'\1_FILES\19_QVD\#template';
LET vStdTaskQvdFilesKc			= vStdPath &'\1_FILES\19_QVD\#kc';
LET vStdTaskQvdTransactData		= vStdPath &'\3_TRANSACT\39_QVD\#data';
LET vStdTaskQvdTransactKc		= vStdPath &'\3_TRANSACT\39_QVD\#kc';


// **************** ****************
// *** file
// **************** ****************

LET vStdTaskQvdCalendarData		= '$(vStdTaskQvdFilesKc)\Kalender.qvd';
LET vStdTaskQvdExchangeData		= '$(vStdPath)\3_TRANSACT\39_QVD\#data\Exchange.Bundesbank.qvd';


// **************** ****************
// *** config
// **************** ****************

IF Match(vStdTaskLevelNo,1,2,3,6) then

	LET vStdTaskConfigProject			= vStdPath &'\0_CONFIG\'& vStdTaskScopePrefix & vStdTaskScope &'\config-'& Lower(vStdTaskLevel) &'-'& vStdTaskPrefix &'.csv';
	LET vStdTaskConfigScope				= vStdPath &'\0_CONFIG\'& vStdTaskScopePrefix & vStdTaskScope &'\config-'& Lower(vStdTaskLevel) &'-'& vStdTaskScope &'.csv';
	LET vStdTaskConfigStandard			= vStdPath &'\0_CONFIG\#standard\config-' & Lower(vStdTaskLevel) & '-' & vStdTaskPrefix &'.csv';

	LET vStdTaskConfigStandardAvailable	= If(FileSize(vStdTaskConfigStandard)>0,1,0);

	IF FileSize(vStdTaskConfigProject)>0 then
		LET vStdTaskConfig			= vStdTaskConfigProject;
		LET vStdTaskConfigType		= 'project';
	ELSEIF FileSize(vStdTaskConfigScope)>0 then
		LET vStdTaskConfig			= vStdTaskConfigScope;
		LET vStdTaskConfigType		= 'scope';
	ELSEIF vStdTaskConfigStandardAvailable=1 then
		LET vStdTaskConfig			= vStdTaskConfigStandard;
		LET vStdTaskConfigType		= 'standard';
	ELSE
		LET vStdTaskConfig			= Null();
		LET vStdTaskConfigType		= 'none';
	ENDIF

ELSE

	LET vStdTaskConfigScope					= Null();
	LET vStdTaskConfigStandard				= Null();
	LET vStdTaskConfigStandardAvailable		= Null();
	LET vStdTaskConfig						= Null();

ENDIF



// **************** **************** **************** ****************;
// *** Variables with Parameters;
// **************** **************** **************** ****************;

IF Match(vStdTaskLevelNo,1,2,3,6) then
	SET vStdFctContentIsLen		= If(Len(Trim($1))>0,$1,Null());
	SET vStdFctContentIsNum		= If(IsNum($1)>0,$1,Null());
ENDIF



// **************** **************** **************** ****************;
// *** Ini;
// **************** **************** **************** ****************;

// Ini
$(Must_Include=$(vStdPath)\9_SYSTEM\90_CONFIG\standard.ini);

LET vStdIniCalendarDateMinNum	= If(IsNum(vStdIniCalendarDateMin),Num(vStdIniCalendarDateMin),Null());
LET vStdIniCalendarDateMaxNum	= If(IsNum(vStdIniCalendarDateMax),Num(vStdIniCalendarDateMax),Null());
LET vStdIniLimitUntilMin		= vStdIniLimitUntilMax*-1;

// Gui
IF vStdTaskLevelNo=5 then
	$(Must_Include=$(vStdPath)\9_SYSTEM\90_CONFIG\gui.ini);
ENDIF

// iProcurement
$(Must_Include=$(vStdPath)\9_SYSTEM\90_CONFIG\iProcurement.ini);
IF FileSize(vStdPath &'\0_CONFIG\'& vStdTaskScopePrefix & vStdTaskScope &'\'& vStdTaskScope &'-iProcurement.ini')>0 then
	$(Include=$(vStdPath)\0_CONFIG\$(vStdTaskScopePrefix)$(vStdTaskScope)\$(vStdTaskScope)-iProcurement.ini);
ELSEIF FileSize(vStdPath &'\0_CONFIG\'& vStdTaskScopePrefix & vStdTaskScope &'\'& vStdTaskScope &'-Einkaufstracker.ini')>0 then
	$(Include=$(vStdPath)\0_CONFIG\$(vStdTaskScopePrefix)$(vStdTaskScope)\$(vStdTaskScope)-Einkaufstracker.ini);
ENDIF



// **************** **************** **************** ****************;
// *** Log;
// **************** **************** **************** ****************;

IF vStdTaskLog then
	LET vStdTaskLog					= True();
	LET vStdTaskLogPath				= Left(DocumentPath(),Index(DocumentPath(),'\',-3)) & vStdTaskLevelNo &'3_LOG\'& vStdTaskScopePrefix & vStdTaskScope;
	LET vStdTaskLogFile				= vStdTaskLogPath &'\'& vStdTaskName &'-'& vStdTimestampNum &'.log';
ELSE
	LET vStdTaskLog					= Null();
ENDIF
