# ******** ps1 - UrlFile ********
# 
# author:		Felix Schmidt
# 
#
# LOG:
# modified		version		description
# 2016-07-22	1.00		created
# 
# ******** ps1 - UrlFile ********


PARAM (
	[string] $Uri,
	[string] $File,
	[string] $User,
	[string] $Pass
)

# review
if ($Uri -and $File) {

	# mkdir if not exists
#	$Dir = (get-item $File).parent
#	if ($Dir) {
#		if (!(Test-Path $Dir)) {
#			New-Item -Path $Dir -ItemType Directory
#		}
#	}

	# login
	if ($User -and $Pass) {
		$r = Invoke-WebRequest $Uri
		$r.Forms[0].Name = $User
		$r.Forms[0].Password = $Pass
		Invoke-RestMethod $Uri -Body $r
	}
	# no login
	else {
		Invoke-WebRequest -Uri $Uri -OutFile $File
		# (New-Object System.Net.WebClient).DownloadFile($Uri,$File)
		# Start-BitsTransfer -Source $Uri -Destination $File -Asynchronous
	}
}
