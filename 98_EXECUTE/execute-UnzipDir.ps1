# ******** ps1 - UnzipDir ********
# 
# author:		Felix Schmidt
# 
#
# LOG:
# modified		version		description
# 2016-07-18	1.00		created
# 
# ******** ps1 - UnzipDir ********


PARAM (
	[string] $ZipFilesPath,
	[string] $UnzipPath
)

# review
if ($ZipFilesPath -and $UnzipPath) {

	# mkdir if not exists
	if (!(Test-Path $UnzipPath)) {
		new-item $UnzipPath -itemtype directory
	}

	$Shell = New-Object -com Shell.Application
	$Location = $Shell.NameSpace($UnzipPath)
	$ZipFiles = Get-Childitem $ZipFilesPath -Recurse -Include *.ZIP

	$progress = 1
	foreach ($ZipFile in $ZipFiles) {

		Write-Progress -Activity "Unzipping to $($UnzipPath)" -PercentComplete (($progress / ($ZipFiles.Count + 1)) * 100) -CurrentOperation $ZipFile.FullName -Status "File $($Progress) of $($ZipFiles.Count)"
		$ZipFolder = $Shell.NameSpace($ZipFile.fullname)

		$Location.Copyhere($ZipFolder.items(), 1040) 
		$progress++
	}
}
